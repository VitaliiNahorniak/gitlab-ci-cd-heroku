// import 'dotenv/config';
import * as studentsModel from '../src/students/students.model';

describe('checkStudents', () => {
  it('First there are two students', () => {
    const students = studentsModel.getAllStudents();

    const result = [
      {
        id: '1',
        name: 'Taras',
        surname: 'Bulba',
        email: 'tarasbulba@gmail.com',
        age: 90,
      },
      {
        id: '2',
        name: 'Ivan',
        surname: 'Franko',
        email: 'ivanfranko@gmail.com',
        age: 77,
      },
    ];

    expect(students).toEqual(result);
  });
});
