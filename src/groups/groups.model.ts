import { getAllStudents } from '../students/students.model';
import { IGroup } from './types/groups.interface';
import ObjectID from 'bson-objectid';

const groups: IGroup[] = [
  {
    id: ObjectID().toHexString(),
    name: 'Kozaks',
  },
  {
    id: ObjectID().toHexString(),
    name: 'Writers',
  },
];

export const getAllGroups = (): IGroup[] => {
  return groups;
};

export const getAllGroupsWithStudents = (): IGroup[] => {
  const allStudents = getAllStudents();
  const allGroupsWithStudents: IGroup[] = [];

  groups.forEach((eachGroup) => {
    const requiredStudents = allStudents.filter(
      ({ groupId }) => groupId === eachGroup.id,
    );

    const groupWithStudents = {
      ...eachGroup,
      students: requiredStudents,
    };

    allGroupsWithStudents.push(groupWithStudents);
  });

  return allGroupsWithStudents;
};

export const getGroupById = (groupId: string): IGroup | undefined => {
  return groups.find(({ id }) => id === groupId);
};

export const getGroupWithStudentsById = (
  groupId: string,
): IGroup | undefined => {
  const group = groups.find(({ id }) => id === groupId);
  const allStudents = getAllStudents();

  if (!group) {
    return;
  }

  const requiredStudents = allStudents.filter(
    ({ groupId }) => groupId === group.id,
  );

  const groupWithStudents = {
    ...group,
    students: requiredStudents,
  };

  return groupWithStudents;
};

export const createGroup = (createGroupSchema: Omit<IGroup, 'id'>): IGroup => {
  const newGroup = {
    id: ObjectID().toHexString(),
    ...createGroupSchema,
  };
  groups.push(newGroup);
  return newGroup;
};

export const updateGroupById = (
  groupId: string,
  updateGroupSchema: Partial<IGroup>,
): IGroup | undefined => {
  const groupIndex = groups.findIndex(({ id }) => id === groupId);
  const group = groups[groupIndex];

  if (!group) {
    return;
  }

  const updatedGroup = {
    ...group,
    ...updateGroupSchema,
  };

  groups.splice(groupIndex, 1, updatedGroup);
  return updatedGroup;
};

export const deleteGroupById = (groupId: string): IGroup | undefined => {
  const groupIndex = groups.findIndex(({ id }) => id === groupId);
  const group = groups[groupIndex];

  if (!group) {
    return;
  }

  groups.splice(groupIndex, 1);
  return group;
};
