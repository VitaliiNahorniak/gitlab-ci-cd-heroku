import { Router } from 'express';
import * as groupsController from './groups.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { groupCreateSchema, groupUpdateSchema } from './groups.schema';
import { idParamsSchema } from '../application/schemas/id-param.schema';

const router = Router();

router.get('/', controllerWrapper(groupsController.getAllGroups));

router.get(
  '/all/withStudents',
  controllerWrapper(groupsController.getAllGroupsWithStudents),
);

router.get(
  '/:id',
  validator.params(idParamsSchema),
  controllerWrapper(groupsController.getGroupById),
);

router.get(
  '/:id/withStudents',
  validator.params(idParamsSchema),
  controllerWrapper(groupsController.getGroupWithStudentsById),
);

router.post(
  '/',
  validator.body(groupCreateSchema),
  controllerWrapper(groupsController.createGroup),
);

router.patch(
  '/:id',
  validator.params(idParamsSchema),
  validator.body(groupUpdateSchema),
  controllerWrapper(groupsController.updateGroupById),
);

router.delete(
  '/:id',
  validator.params(idParamsSchema),
  controllerWrapper(groupsController.deleteGroupById),
);

export default router;
