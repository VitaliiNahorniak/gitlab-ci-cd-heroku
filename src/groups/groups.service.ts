import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import * as groupsModel from './groups.model';
import { IGroup } from './types/groups.interface';

export const getAllGroups = () => {
  return groupsModel.getAllGroups();
};

export const getAllGroupsWithStudents = () => {
  return groupsModel.getAllGroupsWithStudents();
};

export const getGroupById = (id: string) => {
  const group = groupsModel.getGroupById(id);

  if (!group) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }

  return group;
};

export const getGroupWithStudentsById = (id: string) => {
  const groupWithStudents = groupsModel.getGroupWithStudentsById(id);

  if (!groupWithStudents) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }

  return groupWithStudents;
};

export const createGroup = (createGroupSchema: Omit<IGroup, 'id'>) => {
  return groupsModel.createGroup(createGroupSchema);
};

export const updateGroupById = (
  Id: string,
  updateGroupSchema: Partial<IGroup>,
) => {
  return groupsModel.updateGroupById(Id, updateGroupSchema);
};

export const deleteGroupById = (id: string) => {
  return groupsModel.deleteGroupById(id);
};
