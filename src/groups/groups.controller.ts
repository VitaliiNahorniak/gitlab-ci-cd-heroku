import { Request, Response } from 'express';
import * as groupsService from './groups.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IGroupUpdateRequest } from './types/group-update-request.interface';
import { IGroupCreateRequest } from './types/group-create-request.interface';
import { HttpStatuses } from '../application/enums/http-statuses.enum';

export const getAllGroups = (request: Request, response: Response) => {
  const groups = groupsService.getAllGroups();
  response.status(HttpStatuses.OK).json(groups);
};

export const getAllGroupsWithStudents = (
  request: Request,
  response: Response,
) => {
  const groupsWithStudents = groupsService.getAllGroupsWithStudents();
  response.status(HttpStatuses.OK).json(groupsWithStudents);
};

export const getGroupById = (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const group = groupsService.getGroupById(id);
  response.status(HttpStatuses.OK).json(group);
};

export const getGroupWithStudentsById = (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const groupWithStudents = groupsService.getGroupWithStudentsById(id);
  response.status(HttpStatuses.OK).json(groupWithStudents);
};

export const createGroup = (
  request: ValidatedRequest<IGroupCreateRequest>,
  response: Response,
) => {
  const group = groupsService.createGroup(request.body);
  response.status(HttpStatuses.CREATED).json(group);
};

export const updateGroupById = (
  request: ValidatedRequest<IGroupUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const group = groupsService.updateGroupById(id, request.body);
  response.status(HttpStatuses.ACCEPTED).json(group);
};

export const deleteGroupById = (
  request: Request<{ id: string }>, //request: Request
  response: Response,
) => {
  const { id } = request.params;
  const group = groupsService.deleteGroupById(id);
  response.status(HttpStatuses.OK).json(group);
};
