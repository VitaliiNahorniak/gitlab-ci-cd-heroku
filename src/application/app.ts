import express from 'express';
import cors from 'cors';
import logger from './middlewares/logger.middleware';
import studentsRouter from '../students/students.router';
import groupsRouter from '../groups/groups.router';
import bodyParser from 'body-parser';
import exceptionFilter from './middlewares/exceptions.filter';
import path from 'path';

const app = express();

app.use(cors()); //run middleware - solve issue with cors in browser
app.use(bodyParser.json()); //run middleware - understand body from request
app.use(logger); //run middleware - log method

const staticFilesPath = path.join(__dirname, '../', 'public');

app.use('/api/v1/public', express.static(staticFilesPath));
app.use('/api/v1/students', studentsRouter);
app.use('/api/v1/groups', groupsRouter);

app.use(exceptionFilter); //run middleware - catch error

export default app;
