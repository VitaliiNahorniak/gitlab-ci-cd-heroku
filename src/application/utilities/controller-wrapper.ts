import { Request, Response, NextFunction } from 'express';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const controllerWrapper = (requestHandler: any) => {
  return async (request: Request, response: Response, next: NextFunction) => {
    try {
      await requestHandler(request, response, next);
    } catch (error) {
      next(error);
    }
  };
};

export default controllerWrapper;
