import { Request, Response, NextFunction } from 'express';

//middleware
const logger = (request: Request, response: Response, next: NextFunction) => {
    console.log(`>>> ${request.method} ${request.url}`);

    next();
}

export default logger;