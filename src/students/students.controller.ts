import { Request, Response } from 'express';
import * as studentsService from './students.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IStudentUpdateRequest } from './types/student-update-request.interface';
import { IStudentCreateRequest } from './types/student-create-request.interface';
import { IStudentAddGroupIdRequest } from './types/student-addGroupId-request.interface';
import { HttpStatuses } from '../application/enums/http-statuses.enum';

export const getAllStudents = (request: Request, response: Response) => {
  const students = studentsService.getAllStudents();
  response.status(HttpStatuses.OK).json(students);
};

export const getAllStudentsWithGroup = (
  request: Request,
  response: Response,
) => {
  const studentsWithGroup = studentsService.getAllStudentsWithGroup();
  response.status(HttpStatuses.OK).json(studentsWithGroup);
};

export const getStudentWithGroupNameById = (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const student = studentsService.getStudentWithGroupNameById(id);
  response.status(HttpStatuses.OK).json(student);
};

export const getStudentById = (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const student = studentsService.getStudentById(id);
  response.status(HttpStatuses.OK).json(student);
};

export const createStudent = (
  request: ValidatedRequest<IStudentCreateRequest>,
  response: Response,
) => {
  const student = studentsService.createStudent(request.body);
  response.status(HttpStatuses.CREATED).json(student);
};

export const updateStudentById = (
  request: ValidatedRequest<IStudentUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const student = studentsService.updateStudentById(id, request.body);
  response.status(HttpStatuses.ACCEPTED).json(student);
};

export const addImage = (
  request: Request<{ id: string; file: Express.Multer.File }>,
  response: Response,
) => {
  const { id } = request.params;
  const { path } = request.file ?? {};
  const student = studentsService.addImage(id, path);
  response.status(HttpStatuses.CREATED).json(student);
};

export const addGroupIdForStudentById = (
  request: ValidatedRequest<IStudentAddGroupIdRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const student = studentsService.updateStudentById(id, request.body);
  response.status(HttpStatuses.CREATED).json(student);
};

export const deleteStudentById = (
  request: Request<{ id: string }>, //request: Request
  response: Response,
) => {
  const { id } = request.params;
  const student = studentsService.deleteStudentById(id);
  response.status(HttpStatuses.OK).json(student);
};
