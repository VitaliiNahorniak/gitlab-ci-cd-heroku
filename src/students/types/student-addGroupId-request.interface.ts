import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IStudent } from './students.interface';

export interface IStudentAddGroupIdRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Pick<IStudent, 'groupId'>;
}
