export interface IStudent {
  id: string;
  name: string;
  surname: string;
  email: string;
  age: number;
  imagePath?: string;
  groupId?: string;
}
