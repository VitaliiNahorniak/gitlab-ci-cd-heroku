import { Router } from 'express';
import * as studentsController from './students.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
// import validator from '../application/middlewares/validation.middleware';
// import {
//   studentAddGroupIdSchema,
//   studentCreateSchema,
//   studentUpdateSchema,
// } from './student.schema';
// import { idParamsSchema } from '../application/schemas/id-param.schema';
// import uploadMiddleware from '../application/middlewares/upload.middleware';

const router = Router();

router.get('/', controllerWrapper(studentsController.getAllStudents));

// router.get(
//   '/all/withGroupName',
//   controllerWrapper(studentsController.getAllStudentsWithGroup),
// );

// router.get(
//   '/:id',
//   validator.params(idParamsSchema),
//   controllerWrapper(studentsController.getStudentById),
// );

// router.get(
//   '/:id/withGroupName',
//   validator.params(idParamsSchema),
//   controllerWrapper(studentsController.getStudentWithGroupNameById),
// );

// router.post(
//   '/',
//   validator.body(studentCreateSchema),
//   controllerWrapper(studentsController.createStudent),
// );

// router.patch(
//   '/:id',
//   validator.params(idParamsSchema),
//   validator.body(studentUpdateSchema),
//   controllerWrapper(studentsController.updateStudentById),
// );

// //download image
// router.patch(
//   '/:id/image',
//   uploadMiddleware.single('file'),
//   controllerWrapper(studentsController.addImage),
// );

// //Add groupId
// router.patch(
//   '/:id/addGroupId',
//   validator.params(idParamsSchema),
//   validator.body(studentAddGroupIdSchema),
//   controllerWrapper(studentsController.addGroupIdForStudentById),
// );

// router.delete(
//   '/:id',
//   validator.params(idParamsSchema),
//   controllerWrapper(studentsController.deleteStudentById),
// );

export default router;
