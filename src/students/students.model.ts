import { getAllGroups } from '../groups/groups.model';
import { IStudent } from './types/students.interface';
import ObjectID from 'bson-objectid';

const students: IStudent[] = [
  {
    id: '1', //ObjectID().toHexString(),
    name: 'Taras',
    surname: 'Bulba',
    email: 'tarasbulba@gmail.com',
    age: 90,
  },
  {
    id: '2', //ObjectID().toHexString(),
    name: 'Ivan',
    surname: 'Franko',
    email: 'ivanfranko@gmail.com',
    age: 77,
  },
];

export const getAllStudents = (): IStudent[] => {
  return students;
};

export const getAllStudentsWithGroup = (): IStudent[] => {
  const groups = getAllGroups();

  const allStudentsWithGroup: IStudent[] = [];

  students.forEach((student) => {
    if (student.groupId) {
      const requiredGroup = groups.find(({ id }) => id === student.groupId);

      if (!requiredGroup) {
        return;
      }

      const requiredGroupName = requiredGroup.name;

      const { groupId, ...rest } = student; //delete groupId

      const studentWithGroupName = {
        ...rest,
        groupName: requiredGroupName,
      };

      allStudentsWithGroup.push(studentWithGroupName);
    } else {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { groupId, ...rest } = student; //delete groupId

      const studentWithGroupNameNull = {
        ...rest,
        groupName: null,
      };

      allStudentsWithGroup.push(studentWithGroupNameNull);
    }
  });

  return allStudentsWithGroup;
};

export const getStudentById = (studentId: string): IStudent | undefined => {
  return students.find(({ id }) => id === studentId);
};

export const getStudentWithGroupNameById = (
  studentId: string,
): IStudent | undefined => {
  const student = students.find(({ id }) => id === studentId);
  const groups = getAllGroups();

  if (!student) {
    return;
  }

  if (!student.groupId) {
    return;
  }

  const requiredGroup = groups.find(({ id }) => id === student.groupId);

  if (!requiredGroup) {
    return;
  }
  const requiredGroupName = requiredGroup.name;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { groupId, ...rest } = student; //delete groupId

  const studentWithGroupName = {
    ...rest,
    groupName: requiredGroupName,
  };

  return studentWithGroupName;
};

export const createStudent = (
  createStudentSchema: Omit<IStudent, 'id'>,
): IStudent => {
  const newStudent = {
    id: ObjectID().toHexString(),
    ...createStudentSchema,
  };
  students.push(newStudent);
  return newStudent;
};

export const updateStudentById = (
  studentId: string,
  updateStudentSchema: Partial<IStudent>,
): IStudent | undefined => {
  const studentIndex = students.findIndex(({ id }) => id === studentId);
  const student = students[studentIndex];

  if (!student) {
    return;
  }

  const updatedStudent = {
    ...student,
    ...updateStudentSchema,
  };

  students.splice(studentIndex, 1, updatedStudent);
  return updatedStudent;
};

export const deleteStudentById = (studentId: string): IStudent | undefined => {
  const studentIndex = students.findIndex(({ id }) => id === studentId);
  const student = students[studentIndex];

  if (!student) {
    return;
  }

  students.splice(studentIndex, 1);
  return student;
};
