import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import * as studentsModel from './students.model';
import { IStudent } from './types/students.interface';
import ObjectID from 'bson-objectid';
import path from 'path';
import fs from 'fs/promises';

export const getAllStudents = () => {
  return studentsModel.getAllStudents();
};

export const getAllStudentsWithGroup = () => {
  return studentsModel.getAllStudentsWithGroup();
};

export const getStudentById = (id: string) => {
  const student = studentsModel.getStudentById(id);

  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return student;
};

export const getStudentWithGroupNameById = (id: string) => {
  const studentWithGroupName = studentsModel.getStudentWithGroupNameById(id);

  if (!studentWithGroupName) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Student or Group not found',
    );
  }

  return studentWithGroupName;
};

export const createStudent = (createStudentSchema: Omit<IStudent, 'id'>) => {
  return studentsModel.createStudent(createStudentSchema);
};

export const updateStudentById = (
  studentId: string,
  updateStudentSchema: Partial<IStudent>,
) => {
  return studentsModel.updateStudentById(studentId, updateStudentSchema);
};

export const addImage = async (studentId: string, filePath?: string) => {
  if (!filePath) {
    throw new HttpException(HttpStatuses.BAD_REQUEST, 'File is not provided');
  }
  try {
    const imageId = ObjectID().toHexString(); //RandomName
    const imageExtention = path.extname(filePath); //Read Extention (.png or .jpeg or ...)
    const imageName = imageId + imageExtention; //RandomName.png

    const studentsDirectoryName = 'students';
    const studentsDidectoryPath = path.join(
      __dirname,
      '../',
      'public',
      studentsDirectoryName,
    );
    const newImagePath = path.join(studentsDidectoryPath, imageName); //New place for image
    const imagePath = `${studentsDirectoryName}/${imageName}`; //Write in student

    await fs.rename(filePath, newImagePath); //Move the image

    const updatedStudent = updateStudentById(studentId, { imagePath });

    return updatedStudent;
  } catch (error) {
    await fs.unlink(filePath); //delete file if error
    throw error;
  }
};

export const addGroupIdForStudentById = (
  studentId: string,
  updateStudentSchema: Pick<IStudent, 'groupId'>,
) => {
  return studentsModel.updateStudentById(studentId, updateStudentSchema);
};

export const deleteStudentById = (id: string) => {
  return studentsModel.deleteStudentById(id);
};
