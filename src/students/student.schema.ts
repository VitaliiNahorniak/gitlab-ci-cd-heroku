import Joi from 'joi';
import { IStudent } from './types/students.interface';

//when create student, it should be IStudent without id
export const studentCreateSchema = Joi.object<Omit<IStudent, 'id'>>({
  name: Joi.string().required(), // required - must be
  surname: Joi.string().required(),
  email: Joi.string().required(),
  age: Joi.number().required(),
});

//when update student, it should be IStudent optionals
export const studentUpdateSchema = Joi.object<Partial<IStudent>>({
  name: Joi.string().optional(), // optional - be or not
  surname: Joi.string().optional(),

  email: Joi.string().optional(),
  age: Joi.number().optional(),
});

//when add groupId for student
export const studentAddGroupIdSchema = Joi.object<Pick<IStudent, 'groupId'>>({
  groupId: Joi.string().required(),
});
