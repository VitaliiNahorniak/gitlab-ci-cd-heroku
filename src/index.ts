import 'dotenv/config';
import app from './application/app';

const port = process.env.PORT || 8001;

const startServer = async () => {
  app.listen(port, () => {
    console.log(`Server started at port ${port}`);
  });
};

startServer();

/*
app.get('/test/:id', (request: express.Request, response: express.Response) => {
    //response.send('Hello world');
    response.json('Hello world');
    //response.end(); // відповідь без даних
    console.log(request.params.id);
})

app.get('/test', (request: express.Request, response: express.Response) => { //(Endpoint, handler)
    console.log(request.query); //query те що йде після "?" в запиті
    response.json('Hello world');
})

app.all('/test', (request: express.Request, response: express.Response) => { //(Endpoint, handler)
    console.log(request.query); //query те що йде після "?" в запиті
    response.json('Hello world');
})

app.get('/test', (request: express.Request, response: express.Response) => { //(Endpoint, handler)
    response.json('Hello world');
})
*/
